Img info
========
```shell
singularity inspect pipeline.img
```

Listing all apps in Img
=======================
```shell
singularity apps pipeline.img
```

Inspecting all apps in Img
==========================
```shell
for app in $(singularity apps pipeline-dev.img); do singularity inspect --app $app pipeline-dev.img; done
{
    "VERSION": "0.7.17-r1194-dirty",
    "SINGULARITY_APP_NAME": "bwa",
    "SINGULARITY_APP_SIZE": "8MB"
}
{
    "VERSION": "v0.11.5",
    "SINGULARITY_APP_NAME": "fastqc",
    "SINGULARITY_APP_SIZE": "21MB"
}
{
    "VERSION": "v1.2.0",
    "SINGULARITY_APP_NAME": "freebayes",
    "SINGULARITY_APP_SIZE": "276MB"
}
{
    "VERSION": "1.3.2",
    "SINGULARITY_APP_NAME": "htslib",
    "SINGULARITY_APP_SIZE": "42MB"
}
{
    "VERSION": "1.2.1",
    "SINGULARITY_APP_NAME": "manta",
    "SINGULARITY_APP_SIZE": "803MB"
}
{
    "VERSION": "v1.6",
    "SINGULARITY_APP_NAME": "multiqc",
    "SINGULARITY_APP_SIZE": "1MB"
}
{
    "VERSION": "release 2018_06",
    "SINGULARITY_APP_NAME": "ngsbits",
    "SINGULARITY_APP_SIZE": "798MB"
}
{
    "VERSION": "2.17.4",
    "SINGULARITY_APP_NAME": "picard",
    "SINGULARITY_APP_SIZE": "13MB"
}
{
    "VERSION": "dev_latest",
    "SINGULARITY_APP_NAME": "pipeline",
    "SINGULARITY_APP_SIZE": "1MB"
}
{
    "BEDFILE": "CMDL_130217_capture_targets_noChr.bed",
    "REF_FASTA_FILE": "hs37d5_phiX174.fa",
    "SINGULARITY_APP_NAME": "refgenomes",
    "SINGULARITY_APP_SIZE": "11321MB"
}
{
    "VERSION": "3.9.1",
    "SINGULARITY_APP_NAME": "rtgtools",
    "SINGULARITY_APP_SIZE": "90MB"
}
{
    "VERSION": "1.6",
    "SINGULARITY_APP_NAME": "samtools",
    "SINGULARITY_APP_SIZE": "63MB"
}
{
    "VERSION_SNPSIFT": "4.3t",
    "VERSION_SNPEFF": "4.3t",
    "SINGULARITY_APP_NAME": "snpeff",
    "SINGULARITY_APP_SIZE": "16677MB"
}
{
    "VERSION": "2.8.4",
    "SINGULARITY_APP_NAME": "strelka",
    "SINGULARITY_APP_SIZE": "367MB"
}

```

Building IMG
============
```shell
sudo SINGULARITY_TMPDIR=/tmp/ SINGULARITY_CACHEDIR=/cache/ sudo singularity build --writable cce.img ccePipeline.def
```

Run pipeline from IMG
=====================
```shell
singularity run --bind /path_to/,/path_to_A/,/path_to_B/ --app pipeline cce.v3.img \
	/path_to/SAMPLEA-TUMOR_SX_L008_R1_001.fastq.gz \
	/path_to/SAMPLEA-NORMAL_SX_L008_R1_001.fastq.gz \
	/path_to/output/ \
	number_of_CPU_cores \
	SAMPLENAME \
	/path_to/process.log
	/path/to/gnomad_annotation \
	/pth/to/normalreference \
	/path/to/dbnsfp \
	number_of_bases_to_extend_for_snvcalls
```

ARGUMENTS
---------
1. Sample tumor R1 fastq file; R2 fastq file will be determined automatically
2. Sample normal R1 fastq file; R2 fastq file will be determined automatically
3. Main output location. A sub folder with your given samplename will be created there and that will contain all the results
4. Number of CPU cores to use
5. Sample name of your choice
6. output log file
7. gnpmad annotation file
8. normal reference pool for cnvkit
9. dbnsfp annotation file
10. Number of bases to extend for SNV calls

